# Generation multiple orders for miniShop2 #

Class, which generates of multiple orders for miniShop2

miniShop2::addService():


```
#!php

if ($miniShop2 = $modx->getService('miniShop2')) {
    $miniShop2->addService('order', 'msorderhandlercustom',
        '{core_path}components/minishop2custom/msorderhandlercustom.class.php'
    );
}
```

miniShop2::removeService():


```
#!php

if ($miniShop2 = $modx->getService('miniShop2')) {
    $miniShop2->removeService('order', 'msorderhandlercustom');
}
```