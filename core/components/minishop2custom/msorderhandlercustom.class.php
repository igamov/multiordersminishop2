<?php
class msOrderHandlerCustom extends msOrderHandler {
	public function submit($data = array())
		{
			$response = $this->ms2->invokeEvent('msOnSubmitOrder', array(
				'data' => $data,
				'order' => $this,
			));
			if (!$response['success']) {
				return $this->error($response['message']);
			}
			if (!empty($response['data']['data'])) {
					$this->set($response['data']['data']);
			}

			$response = $this->getDeliveryRequiresFields();
			if ($this->ms2->config['json_response']) {
					$response = json_decode($response, true);
			}
			$requires = $response['data']['requires'];

			$errors = array();
			foreach ($requires as $v) {
					if (!empty($v) && empty($this->order[$v])) {
							$errors[] = $v;
					}
			}
			if (!empty($errors)) {
					return $this->error('ms2_order_err_requires', $errors);
			}

			$cart = $this->ms2->cart->get();
			$products = array();
			$partners = array();
			$idOrders = array();
			foreach ($cart as $v) {
			  if(!in_array($v['vendor'],$partners)){
				  $partners[] = $v['vendor'];
			  }
			}
			foreach ($partners as $partner){
				$user_id = $this->ms2->getCustomerId();
				$cart_status = $this->ms2->cart->status();
				$delivery_cost = $this->getCost(false, true);
				$createdon = date('Y-m-d H:i:s');
				$cart_cost = 0;
				$weight_cost = 0;

				// Adding products
				foreach ($this->ms2->cart->get() as $v) {
					if ($partner == $v['vendor']){
						if ($tmp = $this->modx->getObject('msProduct', $v['id'])) {
								$name = $tmp->get('pagetitle');
						} else {
								$name = '';
						}
						/** @var msOrderProduct $product */
						$product = $this->modx->newObject('msOrderProduct');
						$product->fromArray(array_merge($v, array(
								'product_id' => $v['id'],
								'name' => $name,
								'cost' => $v['price'] * $v['count'],
							)));
						$cart_cost = $cart_cost + ($v['price'] * $v['count']); //�������� ��������� ������ ��� ��������
						$weight_cost = $weight_cost + $v['weight'];
						$products[] = $product;
						$key = md5($v['id'] . $v['price'] . $v['weight'] . $v['vendor'] . (json_encode($v['options'])));
						$remove = $this->ms2->cart->remove($key); //������� ����� �� �������
					}
				}
				/** @var msOrder $order */
				$order = $this->modx->newObject('msOrder');
				$order->fromArray(array(
					'user_id' => $user_id,
					'createdon' => $createdon,
					'num' => $this->getNum(),
					'delivery' => $this->order['delivery'],
					'payment' => $this->order['payment'],
					'cart_cost' => $cart_cost,
					'weight' => $weight_cost,
					'delivery_cost' => $delivery_cost,
					'cost' => $cart_cost + $delivery_cost,
					'status' => 0,
					'context' => $this->ms2->config['ctx'],
					'vendor' => $partner,
				));
				// Adding address
				/** @var msOrderAddress $address */
				$address = $this->modx->newObject('msOrderAddress');
				$address->fromArray(array_merge($this->order, array(
					'user_id' => $user_id,
					'createdon' => $createdon,
				)));
				$order->addOne($address);
				$order->addMany($products);
				$response = $this->ms2->invokeEvent('msOnBeforeCreateOrder', array(
					'msOrder' => $order,
					'order' => $this,
				));
				if (!$response['success']) {
					return $this->error($response['message']);
				}

				if ($order->save()) {
					$response = $this->ms2->invokeEvent('msOnCreateOrder', array(
						'msOrder' => $order,
						'order' => $this,
				));
				if (!$response['success']) {
					return $this->error($response['message']);
				}
				if (empty($_SESSION['minishop2']['orders'])) {
					$_SESSION['minishop2']['orders'] = array();
				}
				$_SESSION['minishop2']['orders'][] = $order->get('id');	
				// Trying to set status "new"
				$response = $this->ms2->changeOrderStatus($order->get('id'), 1);
				if ($response !== true) {
					return $this->error($response, array('msorder' => $order->get('id')));
				}
				unset($products);
				} else{
					$this->modx->log(1, 'Oh no, the wand failed to save!');
				}
				$idOrders[] = $order->get('id');
		}
		/*$this->modx->log(1, '$idOrders: '.implode(",", $idOrders));*/
			$this->ms2->cart->clean();
			$this->clean();
			/*$this->modx->sendRedirect(
												$this->modx->context->makeUrl(
														$this->modx->resource->id,
														array('msorder' => $response['data']['msorder'])
												)
										);*/
				
						/** @var msPayment $payment */
						/*if ($payment = $this->modx->getObject('msPayment',
								array('id' => $order->get('payment'), 'active' => 1))
						) {
								$response = $payment->send($order);
								if ($this->config['json_response']) {
										@session_write_close();
										exit(is_array($response) ? json_encode($response) : $response);
								} else {
										if (!empty($response['data']['redirect'])) {
												$this->modx->sendRedirect($response['data']['redirect']);
										} elseif (!empty($response['data']['msorder'])) {
												$this->modx->sendRedirect(
														$this->modx->context->makeUrl(
																$this->modx->resource->id,
																array('msorder' => $response['data']['msorder'])
														)
												);
										} else {
												$this->modx->sendRedirect($this->modx->context->makeUrl($this->modx->resource->id));
										}

										return $this->success();
								}
						} 
						else {*/
			if ($this->ms2->config['json_response']) {
				return $this->success('', array('msorder' => implode(",", $idOrders)));
			} else {
				$this->modx->sendRedirect(
					$this->modx->context->makeUrl(
						$this->modx->resource->id,
						array('msorder' => implode(",", $idOrders))
					)
				);
			}
			return $this->success();
			return $this->error();
		}
}